{
  kapack ? import
    (fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz")
  {}
}:

let
  pkgs = kapack.pkgs;
  pythonPackages = kapack.pythonPackages;
  python = kapack.python;
  R = pkgs.R;
  rPackages = pkgs.rPackages;

  self = rec {
    dev_shell_py = pkgs.mkShell {
      name = "dev-shell-python";
      buildInputs = [
        python
        pythonPackages.ipython
        pythonPackages.virtualenv
        pythonPackages.pylint
      ] ++ battools_py.propagatedBuildInputs;
    };
    dev_shell_r = pkgs.mkShell {
      name = "dev-shell-r";
      buildInputs = with pkgs; [
        R
        rPackages.devtools
        rPackages.roxygen2
      ] ++ battools_r.propagatedBuildInputs;
    };

    battools_py = pythonPackages.buildPythonPackage {
      name = "battools-py-local";
      src = pkgs.lib.sourceByRegex ./python [
        "setup.py"
        "pstate_changes_to_rectangles.py"
        "pstate-changes-to-rectangles"
      ];
      propagatedBuildInputs = with pythonPackages; [
        pandas
        kapack.procset
      ];
    };

    battools_r = rPackages.buildRPackage {
      name = "battools-r-local";
      src = pkgs.lib.sourceByRegex ./. [
        "DESCRIPTION"
        "NAMESPACE"
        "LICENSE"
        "R"
        "R/.*\.R"
      ];
      propagatedBuildInputs = with rPackages; [
        dplyr
        readr
        magrittr
        assertthat
      ];
      buildInputs = with rPackages; [
        R
        devtools
        roxygen2
      ];
      preBuild = ''
        echo 'devtools::document()' | R --vanilla
      '';
    };

    test_shell_py = pkgs.mkShell {
      name = "test-shell-py";
      buildInputs = with pythonPackages; [
        python
        battools_py
        pytest
      ];
    };
    test_shell_r = pkgs.mkShell {
      name = "test-shell-r";
      buildInputs = [
        pkgs.jq
        R
        battools_r
      ];
    };
  };
in
  self
