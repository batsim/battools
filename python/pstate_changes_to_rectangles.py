#!/usr/bin/env python3
'''Convert a pstate change CSV file (as outputted by Batsim) into easy-to-plot time/resources rectangles into a CSV file.'''
import argparse
from collections import namedtuple
import pandas as pd
import procset

RectangleStart = namedtuple('RectangleStart', ['begin', 'pstate', 'resources'])

def pstate_changes_to_rectangles(pstate_changes_df):
    '''Convert a pstate changes dataframe to a rectangle CSV string.'''
    header = 'starting_time,finish_time,resources,pstate'
    lines = [header]

    # initial state is the first line of the input dataframe
    row1 = pstate_changes_df.iloc[0]
    resources = procset.ProcSet.from_str(row1['machine_id'])
    open_rects = [RectangleStart(row1['time'], row1['new_pstate'], resources)]
    del row1

    for _, row in pstate_changes_df[1:].iterrows():
        date = row['time']
        resources = procset.ProcSet.from_str(row['machine_id'])
        new_pstate = row['new_pstate']
        print(date, resources, new_pstate)

        # compute the new state of open_rects, closing rectangles while doing so.
        new_open_rects = []
        for open_rect in open_rects:
            involved_res = open_rect.resources & resources
            if involved_res.count() > 0:
                # close an open rectangle
                lines.append(f'{open_rect.begin},{date},{involved_res},{open_rect.pstate}')
                # open a new rectangle
                new_open_rects.append(RectangleStart(date, new_pstate, involved_res))

            non_involved_res = open_rect.resources - involved_res
            if non_involved_res.count() > 0:
                # keep previously opened rectangle in next state
                new_open_rects.append(RectangleStart(open_rect.begin, open_rect.pstate, non_involved_res))

    return '\n'.join(lines)

def pstate_changes_file_to_rectangles_file(input_pstate_changes_filename, output_rectangles_filename):
    '''Convert a Batsim _pstate_changes.csv file to a rectangle CSV file.'''
    with open(output_rectangles_filename, 'w') as csv_file:
        input_df = pd.read_csv(input_pstate_changes_filename)
        csv_content = pstate_changes_to_rectangles(input_df) + '\n'
        csv_file.write(csv_content)

def main():
    """
    Program entry point.

    Parses the input arguments then calls pstate_changes_file_to_rectangles_file.
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_pstate_changes', type=str, help="The input file (Batsim's _pstate_changes.csv output)")
    parser.add_argument('output_rectangles', type=str, help='The output CSV file')
    args = parser.parse_args()

    pstate_changes_file_to_rectangles_file(args.input_pstate_changes, args.output_rectangles)

if __name__ == "__main__":
    main()
