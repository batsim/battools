#!/usr/bin/env python
from distutils.core import setup

setup(name='battools',
    version='0.1.0',
    py_modules=['pstate_changes_to_rectangles'],
    scripts=['pstate-changes-to-rectangles'],

    python_requires='>=3.6',
    install_requires=[
        'pandas>=0.24.1',
        'procset>=1.0',
    ],

    description='Set of tools around the Batsim simulator',
    author='Millian Poquet',
    author_email='millian.poquet@inria.fr',
    url='https://framagit.org/batsim/battools',
    license='LGPL-3.0',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
    ],
)
